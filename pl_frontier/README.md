pl_frontier_thg
---------------

The map was decompiled with BSPSource 1.4.3 from the official pl_fronteer_final

Changes
=======
 - thg1
  - The spawn room on final for RED has been fixed to disallow building. 
  - The rocks on the final corner can not be built upon.
